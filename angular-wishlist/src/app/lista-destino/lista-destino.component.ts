import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {
  destino: DestinoViaje[];
  constructor() {
    this.destino = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    this.destino.push(new DestinoViaje (nombre, url));
    return false;
  }

}
